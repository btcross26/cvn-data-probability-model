"""Python Classes for Charpy Analysis"""

# module imports
import math
import numpy as np
import pandas as pd
import scipy.optimize
import scipy.stats
import matplotlib.pyplot as plt
import copy

class CharpyAnalysis(object):
    """Class contains separate methods for analyzing charpy data"""
    def __init__(self, data, pred_sims=10000, bs_trials=1000, integration_pts=1001, \
        half_width_sds=10.0, bs_window=5, units=["Celsius","ft-lb"]):
        self.data = copy.deepcopy(data)
        self.pred_sims = pred_sims
        self.bs_trials = bs_trials
        self.integration_pts = integration_pts   # should be an odd number due to use of simpson's rule
        self.half_width_sds = half_width_sds
        self.objfunc = NLogLikelihood(data, self.integration_pts, self.half_width_sds)
        self.results = CharpyResults(units)
        self.set_bs_index(bs_window)
        self.p_bounds = None
        self.optim_control = {}

    @staticmethod
    def charpy_sigmoid(t, a1, a2, a3, T0):
        """
        Function takes t=temperature along with parameters of 4-parameter logistic
        and returns corresponding Charpy energy value
        """
        energy = (a1 / 2) * (1 - np.tanh((t - T0) / a3)) + (a2 / 2) * (1 + np.tanh((t - T0) / a3))
        return energy
		
    def fit_model(self, params, p_bounds, optim_control={}):
        """
        Fit the Charpy Model to self.data with optim_control controlling
        optimization parameters and p_bounds as list parameter bound tuples
        The L-BFGS-B method is used for optimization.
        """
        print "Fitting Charpy model......"
        results = {}
        self.p_bounds = p_bounds
        self.optim_control = optim_control
        for i in range(1,12):
            print "Iteration:", i
            p0 = np.array(params)
            p0[5] = p0[5] + p0[4] * ((i - 1) / 5.0 - 1)
            result = scipy.optimize.minimize(self.objfunc, x0=p0, method="L-BFGS-B", \
                jac=False, bounds=self.p_bounds, options=optim_control)
            results[i] = result
        self.results.model_fits = results
        min_log_likelihood, min_key = float("inf"), 1
        for key, value in results.items():
            if value["success"] and value["fun"] < min_log_likelihood:
                min_log_likelihood = value["fun"]
                min_key = key
        self.results.best_model = results[min_key]
		
    def pred_int_sims(self, seed=1):
        """
        Function uses parametric bootstrapping of fitted model to determine
        prediction intervals
        Note: These are not true prediction intervals, but rather an estimate based on the orginal
        fit to the data which suffices for the time being.  Still need to implement true prediction intervals
        based on the bootstrapped fits.
        """
        assert self.results.best_model, "self.results.best_model is invalid"
        print "\nSimulating CVN values via parametric bootstrapping......"
        np.random.seed(seed)
        temp_min = np.floor(self.data.iloc[:,0]).min() - 50
        temp_max = np.ceil(self.data.iloc[:,0]).max() + 50
        temp_range = np.arange(temp_min, temp_max + 1)
        p = self.results.best_model["x"]
        sim_t = np.tile(temp_range, self.pred_sims).reshape(self.pred_sims, len(temp_range))
        sim_T0 = np.random.normal(loc=p[5], scale=p[4], size=(self.pred_sims, len(temp_range)))
        sim_epsilon = np.random.lognormal(0.0, math.sqrt(math.log(1 + p[3]**2)), \
            size=(self.pred_sims, len(temp_range)))
        sim_values = self.charpy_sigmoid(sim_t, p[0], p[1], p[2], sim_T0) * sim_epsilon
        self.results.pred_int_sims = pd.DataFrame(sim_values, index=np.arange(1, self.pred_sims + 1), \
            columns=temp_range)
        		
    def bootstrap_params(self, optim_control=None, seed=1):
        """
        Function utilizes bootstrap resampling to determine parameter statistics
        """
        assert self.results.best_model, "self.results.best_model is invalid"
        print "\nSimulating model parameters......"
        if optim_control is None:
            optim_control = self.optim_control
        np.random.seed(seed)
        temp_min = np.floor(self.data.iloc[:,0]).min()
        temp_max = np.ceil(self.data.iloc[:,0]).max()
        bs_samples = np.zeros((self.bs_trials, len(self.data), 2))
        par_sims = np.zeros((self.bs_trials, 6))
        counter = 0
        while counter < self.bs_trials:
            print "Bootstrap sample " + str(counter + 1)
            while True:
                p0 = self.results.best_model["x"]
                resample = self._bs_resample()
                objfunc = NLogLikelihood(resample, self.integration_pts, self.half_width_sds)
                result = scipy.optimize.minimize(objfunc, x0=p0, method="L-BFGS-B", \
                    jac=False, bounds=self.p_bounds, options=optim_control)
                if result.success:
                    bs_samples[counter,:,0] = resample.iloc[:,0]
                    bs_samples[counter,:,1] = resample.iloc[:,1]
                    par_sims[counter,:] = result["x"]
                    counter += 1
                    break
        bs_samples_temp = pd.DataFrame(bs_samples[:,:,0], index=np.arange(1, self.bs_trials + 1))
        bs_samples_energy = pd.DataFrame(bs_samples[:,:,1], index=bs_samples_temp.index)
        self.results.bootstrap_samples = {"temperature":bs_samples_temp, "energy":bs_samples_energy}
        self.results.bootstrap_params = pd.DataFrame(par_sims, index=np.arange(1, self.bs_trials + 1), \
            columns=["a1", "a2", "a3", "Vep", "sT0", "muT0"])
                    
    def _bs_resample(self):
        """
        Creates and returns a stratified sample based on bs_window
        """
        groups = self.data.groupby("bs_index")
        sample_df = groups.transform(lambda df: df.ix[np.random.choice(df.index, len(df)),:])
        return sample_df
        
    def run_all(self, p0, p_bounds, optim_control={}, seed=1):
        """
        Runs a full analysis, but only fits a new model if self.results.best_model is empty
        """
        self.fit_model(p0, p_bounds, optim_control)
        self.pred_int_sims(seed)
        self.bootstrap_params(seed)
        
    def clear_model_results(self, keep_model_fit=False):
        """
        Clears the object model results object (keeps fitted model if keep_model_fit is True
        """
        if keep_model_fit:
            temp1, temp2 = self.results.model_fits, self.results.best_model
            self.results = charpyResults()
            self.results.model_fits, self.results.best_model = temp1, temp2
        else:
            self.results = charpyResults()
			
    def set_pred_sims(self, value):
        """
        Method changes value of pred_sims
        """
        assert type(value) is int and value > 0, "Specified value must be a positive integer"
        self.pred_sims = value
	
    def set_bs_trials(self, value):
        """
        Method changes value of bs_trials
        """
        assert type(value) is int and value > 0, "Specified value must be a positive integer"
        self.bs_trials = value
        
    def set_p_bounds(self, value):
        """
        Method changes value of model fitting constraints
        """
        self.p_bounds = value
        
    def set_optim_control(self, value):
        """
        Method changes value of optim_control
        """
        self.optim_control = value
        
    def set_bs_index(self, bs_window):
        """
        Method to change bootstrap resampling window
        """
        assert bs_window > 0, "Specified value must be positive"
        temp_min = np.floor(self.data.iloc[:,0]).min()
        temp_max = np.ceil(self.data.iloc[:,0]).max()
        temp_bins = np.arange(temp_min, temp_max + bs_window, bs_window)
        self.data["bs_index"] = np.digitize(self.data.iloc[:,0], temp_bins)
        self.results.data = self.data.copy(deep=True)
        
			
class CharpyResults(object):
    """Class contains the results of CharpyAnalysis methods"""
    def __init__(self, units):
        self.units = units
        self.data = None
        self.model_fits = None
        self.best_model = None
        self.pred_int_sims = None
        self.bootstrap_samples = None
        self.bootstrap_params = None
    
    def predint(self, alpha):
        """
        Returns the (1 - alpha)% prediction interval for a series of temperatures
        """
        assert alpha >= 0 and alpha <= 1.0, "<arg>:alpha value must be between 0 and 1"
        lower = self.pred_int_sims.quantile(alpha / 2.0, axis=0)
        upper = self.pred_int_sims.quantile(1.0 - alpha / 2.0, axis=0)
        return pd.DataFrame({"lower":lower, "upper":upper})
    
    def confint_curve(self, alpha):
        """
        Returns the (1 - alpha)% confidence interval for the median curve through
        the mean ductile-brittle transition temperatures
        """
        assert alpha >= 0 and alpha <= 1.0, "<arg>:alpha value must be between 0 and 1"
        tvalues = np.array(self.pred_int_sims.columns, dtype=np.float64)
        evalues = np.zeros((len(self.bootstrap_params), len(tvalues)))
        for ind in range(len(tvalues)):
            temp = tvalues[ind]
            evalues[:,ind] = self.bootstrap_params.apply(lambda p: CharpyAnalysis.charpy_sigmoid(temp, \
                p[0], p[1], p[2], p[5]), axis=1, raw=True)
        evalues = pd.DataFrame(evalues, index=np.arange(1, len(self.bootstrap_params) + 1), \
            columns=tvalues)
        lower = evalues.quantile(alpha / 2.0, axis=0)
        upper = evalues.quantile(1.0 - alpha / 2.0, axis=0)
        return pd.DataFrame({"lower":lower, "upper":upper}, index=tvalues)
        
    def confint_params(self, alpha):
        """
        Returns the (1 - alpha)% confidence interval for the estimated parameters
        """
        assert alpha >= 0 and alpha <= 1.0, "<arg>:alpha value must be between 0 and 1"
        lower = self.bootstrap_params.quantile(alpha / 2.0, axis=0)
        upper = self.bootstrap_params.quantile(1.0 - alpha / 2.0, axis=0)
        return pd.DataFrame({"lower":lower, "upper":upper}, index=self.bootstrap_params.columns)

    def cor_params(self):
        """
        Returns the estimated parameter correlation matrix
        """
        cor_matrix = np.corrcoef(np.array(self.bootstrap_params), rowvar=0)
        labels = self.bootstrap_params.columns
        return pd.DataFrame(cor_matrix, index=labels, columns=labels)
    
    def write_to_files(self):
        self.bootstrap_samples["temperature"].to_csv("bs_temp.csv", header=False, index=False)
        self.bootstrap_samples["energy"].to_csv("bs_energy.csv", header=False, index=False)
        self.pred_int_sims.to_csv("pred_int_sims.csv", header=True, index=False)
        with open("best_model.txt", "w") as f:
            f.write(str(self.best_model))
                    
    def plot(self, pi_alpha=None, ci_alpha=None, xlim=None, ylim=None, lwd=1.5):
        """
        Plots results on scatter plot with prediction and confidence intervals
        Note the intervals can be suppressed by setting corresponding args to None
        """
        # Calculate median curve values to be plotted
        p = self.best_model["x"]
        tmin = np.floor(self.data.iloc[:,0]).min() - 10
        tmax = np.ceil(self.data.iloc[:,0]).max() + 10
        tvalues = np.arange(tmin, tmax + 1)
        evalues = CharpyAnalysis.charpy_sigmoid(tvalues, p[0], p[1], p[2], p[5])
        
        # Create plot using matplotlib.pyplot
        plt.figure()
        ax1 = plt.subplot(111)
        ax1.plot(self.data.iloc[:,0], self.data.iloc[:,1], marker="o", color="k", \
            markerfacecolor="None", linestyle="None", lw=lwd)
        ax1.plot(tvalues, evalues, color="k", linestyle="solid", label="Median Fitted Curve")
        if ci_alpha:
            conf = self.confint_curve(ci_alpha)
            ax1.plot(conf.index, conf.lower, color="b", linestyle="dashed", lw=lwd, \
                label=str(int(100 - 100 * ci_alpha)) + "% Median Curve Confidence Interval")
            ax1.plot(conf.index, conf.upper, color="b", linestyle="dashed", lw=lwd)
        if pi_alpha:
            pred = self.predint(pi_alpha)
            ax1.plot(pred.index, pred.lower, color="r", linestyle="dotted", lw=lwd, \
                label=str(int(100 - 100 * pi_alpha)) + "% Prediction Interval")
            ax1.plot(pred.index, pred.upper, color="r", linestyle="dotted", lw=lwd)
        if xlim:
            ax1.set_xlim(xlim)
        if ylim:
            ax1.set_ylim(ylim)
        plt.xlabel("Temperature (" + self.units[0] + ")")
        plt.ylabel("Energy (" + self.units[1] + ")")
        plt.title("CVN Transition Curve")
        if pi_alpha or ci_alpha:
            plt.legend(loc="best")
        plt.show()
        raw_input("Please press enter to continue...")
        plt.close()
        
    def param_hist(self, param, bins=10, range=None, xlim=None, ylim=None):
        """
        Plots a histogram for the specified param
        <arg>:param should be a string of either "a1", "a2", "a3", "Vep", "sT0", or "muT0"
        """
        par_names = self.bootstrap_params.columns
        if param not in par_names:
            print "Invalid string specification for <arg>:param"
            return None
       
        # Create histogram
        plt.figure()
        ax1 = plt.subplot(111)
        ax1.hist(np.array(self.bootstrap_params[param]), bins, range, normed=1.0, color="r")
        if xlim:
            ax1.set_xlim(xlim)
        if ylim:
            ax1.set_ylim(ylim)
        plt.xlabel("Parameter Value")
        plt.ylabel("Probability")
        plt.title(param + " Histogram")
        plt.show()
        raw_input("Please press enter to continue...")
        plt.close()

        
class NLogLikelihood(object):
    """Class contains the negative log-likelihood function for Charpy analysis"""
    def __init__(self, data, integration_pts, half_width_sds):
        self.data = data
        self.params = None
        self.integration_pts = integration_pts
        self.integration_wts = np.array([1,4] + ((self.integration_pts - 3 ) / 2) * [2,4] + [1])
        self.spread = half_width_sds
		
    def __call__(self, params):
        """
        Class call-by-name method returns negative log-likelihood for data given params set
        """
        self.params = params
        values = self.data.iloc[:,0:2].apply(self._integrateT0, axis=1, NLLobject=self)
        return -1.0 * np.sum(np.log(values))
	
    @staticmethod
    def _integrateT0(data_point, NLLobject):
        """
        Function integrates to obtain marginal distribution of E given t
        Returns:
        Value of pdf for given data point
        Method is static to allow use of <fnc>pd.DataFrame.apply in __call__ method
        """
        p = NLLobject.params
        
        # specify integration upper limit, lower limit, and Simpson weights
        # integration is performed using 101 data points between the limits of
        # -6 standard deviations to +6 sd's around the mean of T0
        spread = NLLobject.spread * p[4]
        a, b = p[5] - spread, p[5] + spread
        no_points = NLLobject.integration_pts
        int_weights = NLLobject.integration_wts
        
        # specify integrand and return numerical integration value
        t_i, E_i = data_point
        T0 = np.linspace(a, b, num=no_points)
        E_m = CharpyAnalysis.charpy_sigmoid(t_i, p[0], p[1], p[2], T0)
        pdf_E_cond = scipy.stats.lognorm(math.sqrt(math.log(1 + p[3]**2)), scale=E_m).pdf(E_i)
        pdf_T0 = scipy.stats.norm(loc=p[5], scale=p[4]).pdf(T0)
        integral = (b - a) / (3.0 * (no_points - 1)) * np.sum(int_weights*pdf_E_cond*pdf_T0)		
        if integral == 0:
            integral = 1e-300
        return integral