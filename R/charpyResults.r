# source additional functions
# source("charpyResultsConfint.r")

### analyzeCharpyResults
### Function returns transition curve analysis results for given data.frame temp_energy_data
### which includes temperature in first column and energy in second column.  params_start
### should be initial estimates of transition curve parameters.  Note that the function
### may fail depending on the initial estimates.  There should be no missing values in the data.
analyzeCharpyResults <- function(temp_energy_data, params_start, lower=c(1e-5,1e-5,2.5,1e-5,1e-5,-Inf),
   bs_trials=1000, pred_sims=10000, seed=1, model_only=FALSE, optim_control=list()) {
   # params = c(a1, a2, a3, V_ep, sigma_T0, mu_T01)
   # a1, a2, a3 as defined in charpySigmoid function <CS> given below
   
   ### Define functions for use in analysis
   # function to create likelihood function that is minimized
   create.charpyNegativeLogLikelihood <- function(data) {
      f <- function(params) {
         require(pracma)
         p <- params
         integrateT0 <- function(t_i, E_i) {
            # specify integration limits and calculate Simpson integration weights (201 pts)
            spread <- 8.0
            a <- p[6] - spread*p[5]
            b <- p[6] + spread*p[5]
            no_points <- 1001
            int_weights <- c(1,4,rep(c(2,4), (no_points-3)/2), 1)   # Simpson rule weights
            
            # return numerical integration value
            T0 <- seq(a, b, length=no_points)
            integral <- (b - a)/(3*(no_points-1))*sum(integrand(T0, t_i, E_i)*int_weights)
            if(integral == 0) {integral <- 1e-300}
            return(integral)            
         }
         
         integrand <- function(T0, t_i, E_i) {
            E_m <- (p[1]/2)*(1 - tanh((t_i - T0)/p[3])) + (p[2]/2) * (1 + tanh((t_i - T0)/p[3]))
            pdf_E_cond <- dlnorm(E_i, log(E_m), sqrt(log(1 + p[4]^2)))
            pdf_T0 <- dnorm(T0, p[6], p[5])
            value <- pdf_T0*pdf_E_cond
            return(value)
         }
         
         icomp <- log(apply(data, 1, function(x) {return(integrateT0(x[1], x[2]))}))
         return(-1*sum(icomp))          
      }
      return(f)   # return created function
   }
   
   # Charpy sigmoid function given values of T, a1, a2, T0, and a3
   CS <- function(T, a1, a2, T0, a3) {
      require(pracma)
      value <- a1/2 * (1 - tanh((T-T0)/a3)) + a2/2 * (1 + tanh((T-T0)/a3))
      return(value)
   }

   ### Beginning of Results Analysis
   # h = function to minimize during optimization
   h <- create.charpyNegativeLogLikelihood(temp_energy_data)

   # fit model for various values of T0 and choose value with minimum neg. log likelihood
   cat("Fitting model to Charpy data...\n")
   start_time <- proc.time()
   constraint_lower <- lower
   model_list <- NULL
   for(i in 1:11) {
      cat("Model fitting iteration ", i, "...\n", sep="")
      params <- params_start
      n <- length(params)
      params[n] <- params[n] + params[n-1]*((i-1)/5 - 1)
      try({mod <- optim(params, h, method="L-BFGS-B", lower=constraint_lower, control=optim_control);
           model_list <- c(model_list,list(mod))}, silent=TRUE)
   }
   if(length(model_list) == 0) {
      stop("Model fitting failed! Starting parameters may be insufficient.")
   }
   min_values <- lapply(model_list, function(x) {return(x$value)})
   model_fit <- model_list[[which.min(min_values)]]
   finish_time <- proc.time()
   cat("Model fitting complete!\n")
   cat("Time: ", (finish_time-start_time)[3], " seconds\n\n", sep="")
   if(model_only) {
      return(model_fit)
   }

   # parametric bootstrap sampling
   # simulate 10000 values at each degree in temp_range
   # Note: Only an estimate.  Samples should be based on bootstrapped parameters.  Needs updating.
   cat("Calculating prediction intervals...\n")
   start_time <- proc.time()
   set.seed(seed)
   temp_min <- floor(min(temp_energy_data[,1])) - 10
   temp_max <- ceiling(max(temp_energy_data[,1])) + 10
   temp_range <- temp_min:temp_max
   p <- model_fit$par
   sims <- NULL
   for(i in 1:pred_sims) {
      sim_T0 <- rnorm(length(temp_range), p[6], p[5])
      sim_epsilon <- rlnorm(length(temp_range), 0, sqrt(log(1+p[4]^2)))
      sim_values <- CS(temp_range, p[1], p[2], sim_T0, p[3])
      sim_values <- sim_values * sim_epsilon
      sims <- rbind(sims, sim_values)
   }   
   pred_int <- apply(sims, 2, quantile, probs=c(0.025,0.05,0.25,0.75,0.95,0.975))
   attr(pred_int,"dimnames")[[2]] <- temp_range
   attr(sims,"dimnames")[[2]] <- temp_range
   finish_time <- proc.time()
   cat("Calculation of prediction intervals complete!\n")
   cat("Time: ", (finish_time-start_time)[3], " seconds\n\n", sep="")

   # calculate 95% confidence intervals for fitted parameters
   cat("Calculating confidence intervals for fitted parameters...\n")
   start_time <- proc.time()
   set.seed(seed)
   params_list <- list()
   samp_list <- list()
   counter <- 1
   while(counter <= bs_trials) {
      cat("Bootstrap sample ", counter, "...\n", sep="")
      samp <- NULL
      for(temp in unique(temp_energy_data[,1])) {
         population <- temp_energy_data[temp_energy_data[,1] == temp,]
         sample_size <- nrow(population)
         samp <- rbind(samp, population[sample(nrow(population), sample_size, replace=TRUE),])
      }
      g <- create.charpyNegativeLogLikelihood(samp)
      try({mod <- optim(p, g, method="L-BFGS-B", lower=constraint_lower, control=optim_control);
           params_list[[counter]] <- mod$par;
           samp_list[[counter]] <- samp;
           counter <- counter + 1}, silent=TRUE)
      #mod <- optim(p, g, method="L-BFGS-B", lower=constraint_lower)
      #params_list[[i]] <- mod$par
      #samp_list[[i]] <- samp
   }
   bs_params <- structure(as.data.frame(t(data.frame(params_list))), names=c("a1","a2","a3",
      "Vep","sigmaT0","muT0"))
   attr(bs_params,"row.names") <- 1:nrow(bs_params)
   confint_params <- apply(bs_params, 2, quantile, probs=c(0.025,0.975))
   finish_time <- proc.time()
   cat("Calculation of parameter confidence intervals complete!\n")
   cat("Time: ", (finish_time-start_time)[3], " seconds\n\n", sep="")

   # calculate 95% error bounds for median curve through mean temperature
   cat("Calculating 95% confidence intervals for median curve given t=T0...\n")
   start_time <- proc.time()
   bounds <- NULL
   for(temp in temp_range) {
      col <- CS(temp, bs_params$a1, bs_params$a2, bs_params$muT0, bs_params$a3)
      bounds <- cbind(bounds, col)
   }
   confint_median_curve <- apply(bounds, 2, quantile, probs=c(0.025,0.975))
   attr(confint_median_curve,"dimnames")[[2]] <- temp_range
   finish_time <- proc.time()
   cat("Calculation of 95% bounds for median curve given t=T0 complete!\n")
   cat("Time: ", (finish_time-start_time)[3], " seconds\n\n", sep="")
   
   ### structure and return the results in a list of class "charpyResults"
   charpyResults <- list(data=temp_energy_data, model_fit=model_fit,
      confint_median_curve=confint_median_curve, confint_params=confint_params,
      bootstrap_params=bs_params, bootstrap_samples=samp_list, pred_int=pred_int,
      pred_int_sims=sims, negLogLikeFunc=h, charpySigFunc=CS)
   class(charpyResults) <- "charpyResults"
   return(charpyResults)
}